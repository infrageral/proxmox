# NAT 1:1 para contêiners - IPv4/6 #

Instrução de como gerar um mapeamento IPv4 e IPv6 QUANDO não for possível o uso de BRIDGE

---

## IPv4 (NAT em modo NETMAP) ##

Observação/Cuidados

Neste modelo, `NETMAP`, a `network` inteira será mapeada (se comportando como bridge), porém, o IP do `HOST` deve ser tratado como exceção para não ser mapeado para a LAN.

Esta exceção é feita, também, pelo `iptables`, através de um `ACCEPT`, na tabela `PREROUTING`, no topo das CHAINS (regra nº 1).

/etc/network/interface
```bash

# interface da WAN
iface <wan> inet static
    address <ipv4-do-host>
    netmask <mask>
    gateway <ipv4-gateway>
    dns-nameservers <dns1> <dns2> ...<dnsN>
    
    # habilitando o FORWARD
    post-up echo 1 > /proc/sys/net/ipv4/ip_forward
    # default Policy
    post-up iptables -P FORWARD -j ACCEPT
    post-down iptables -D FORWARD -j ACCEPT
     
    # exceção de MAPPER ao(s) IP(s) do HOST
    post-up iptables -t nat -I PREROUTING 1 -i <wan> -d <ipv4-do-host>/32 -j ACCEPT
    post-down iptables -t nat -D PREROUTING 1 -i <wan> -d <ipv4-do-host>/32 -j ACCEPT
    
    # todos os IPs que serão exclusivamente de CONTAINERS
    # serão adicionados à intervace WAN.
    # Cada IP WAN será traduzido (mapeado) para um IP LAN correspondente
    post-up ip address add <ipv4-1>/<mask> dev <wan>
    post-up ip address add <ipv4-2>/<mask> dev <wan>
    #    .
    #    .
    #    .
    post-up ip address add <ipv4-N>/<mask> dev <wan>
    
# iface da BRIDGE (LAN)
iface <bridge> inet static
    address <ipv4-da-bridge>
    netmask <mask>
    
    # burlando um problema onde os contêineres PINGam no HOST, mas não conseguem sair para WAN
    # quando faz uso do FIREWALL do PROXMOX (habilitado no próprio contêiner)
    post-up iptables -t raw -A PREROUTING -i fwbr+ -j CT --zone 1
    post-down iptables -t raw -D PREROUTING -i fwbr+ -j CT --zone 1
    
    # NATs de forma MAPEADA
    # traduções:
    #     WAN -> LAN (PREROUTING)
    #     LAN -> WAN (POSTROUTING)
    post-up iptables -t nat -A PREROUTING -i <wan> -d <wan-network>/<wan-mask> -j NETMAP --to <bridge-network>/<bridge-mask>
    post-up iptables -t nat -A POSTROUTING -o <wan> -s <bridge-network>/<bridge-mask> -j NETMAP --to <wan-network>/<wan-mask>
    post-down iptables -t nat -D PREROUTING -i <wan> -d <wan-network>/<wan-mask> -j NETMAP --to <bridge-network>/<bridge-mask>
    post-down iptables -t nat -D POSTROUTING -o <wan> -s <bridge-network>/<bridge-mask> -j NETMAP --to <wan-network>/<wan-mask>
    
```

---


## IPv6 (proxy_ndp) ##

O `proxy_ndp` servirá apenas para que o IPv6, dado ao contêiner, seja acessível da Internet (WAN).
Esta configuração não é obrigatória caso o contêiner apenas faça acesso à Internet.

Obs.: Caso o tamanho do `prefixo` na `WAN` seja `/64` (menor rede disponível), será necessário trabalhar com um prefixo ainda menor (/65, /66, /80...)
na interface da `LAN`.
O motivo é gerar duas REDEs diferentes entre `WAN` e `LAN`, mesmo que prefixo menor que /64 não seja recomendado.



/etc/network/interface
```bash

# interface da WAN
iface <wan> inet6 static
    address <ipv6-do-host>
    netmask <mask>
    gateway <ipv6-gateway>
    dns-nameservers <dns1> <dns2> ...<dnsN>
    
    # habilitando o FORWARD
    post-up echo 1 > /proc/sys/net/ipv6/conf/all/proxy_ndp
         
    # todos os IPs que serão exclusivamente dos CONTAINERS
    # serão adicionados à intervace WAN.
    # PROXY NEIGHbor
    post-up ip -6 neigh add proxy <ipv6-1>[/<mask-da-bridge>] dev <wan>
    post-up ip -6 neigh add proxy <ipv6-2>[/<mask-da-bridge>] dev <wan>
    #    .
    #    .
    #    .
    post-up ip -6 neigh add proxy <ipv6-N>[/<mask-da-bridge>] dev <wan>
    
# iface da BRIDGE (LAN)
iface <bridge> inet6 static
    # mesmo endereço de rede da WAN, porém, com prefixo ligeiramente menor e IP diferente da WAN/Gateway
    address <wan-subnet_as_ipv6-da-bridge>
    # WAN-prefix + 1+n
    netmask <mask>
    
```