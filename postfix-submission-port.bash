#!/bin/bash
#
## acesse
## http://smtplw.com.br/"
#
[[ $UID -ne 0 ]] && echo "Script deve ser executado como ROOT" && exit 1

SMTP_SERVER="smtplw.com.br"
SMTP_PORT="587"
SMTP_USER="user"             # < INFORMAR O USUARIO SMTP
SMTP_PASS="*****"            # < INFORMAR A SENHA SMTP
SMTP_TLS="no"
SMTP_DOMAIN="unimedpalmas.com.br"
## corrige o ROOT (em 'TO')'
#  de:            root@$SMTP_DOMAIN
#  para: $SMTP_MAIL_TO@$SMTP_DOMAIN
SMTP_MAIL_TO="servidores" ## @{SMTP_DOMAIN}

## corrige o ROOT (em 'FROM')
#  de:              root@$SMTP_DOMAIN
#  para: $SMTP_MAIL_FROM@$SMTP_DOMAIN
SMTP_MAIL_FROM=$(hostname -s | awk '{print tolower($0)}')

## iniciando

ARQUIVO="/etc/postfix/main.cf"
[ ! -f ${ARQUIVO} ] && echo "Postfix não está instalado: ${ARQUIVO}" && exit 1;

echo "Configurando nome do ROOT para: root.${SMTP_MAIL_FROM}"
usermod -c "root.${SMTP_MAIL_FROM}" root

echo "Configurando /etc/mailname"
echo "${SMTP_DOMAIN}" > /etc/mailname

## demais configs
function alt_d {
 # $1 key, $2 sep, $3 value, $4 arquivo
 KEY=$(printf '%s\n' "$1" | sed -e 's/[\/&$.]/\\&/g')
 SEP=$(printf '%s\n' "$2" | sed -e 's/[\/&$.]/\\&/g')
 VALUE=$(printf '%s\n' "$3" | sed -e 's/[\/&$.]/\\&/g')

 [ ! -f ${4} ] && return 1;

 ## DEBUG
 #echo "${3} || ${VALUE}";
 #grep -q "^${KEY}${SEP}.*" ${4} && sed -n "s/^${KEY}${SEP}.*/${KEY}${SEP}${VALUE}/p" ${4} || echo "${1}${2}${3}";
 grep -q "^${KEY}${SEP}.*" ${4} && sed -i "s/^${KEY}${SEP}.*/${KEY}${SEP}${VALUE}/" ${4} || echo "${1}${2}${3}" >> ${4};
}

echo "Configurando ${ARQUIVO}"
alt_d "myhostname" " = " "$(hostname -s).${SMTP_DOMAIN}" ${ARQUIVO}
alt_d "smtp_use_tls" " = " "${SMTP_TLS:-no}" ${ARQUIVO}
alt_d "smtp_sasl_auth_enable" " = " "yes" ${ARQUIVO}
alt_d "smtp_sasl_security_options" " = " "noanonymous" ${ARQUIVO}
alt_d "smtp_sasl_password_maps" " = " "hash:/etc/postfix/sasl/smtplw_passwd" ${ARQUIVO}

alt_d "relayhost" " = " "[${SMTP_SERVER}]:${SMTP_PORT}" ${ARQUIVO}

if [ "${SMTP_TLS}" = "yes" ];then
 alt_d "smtp_tls_security_level" " = " "encrypt" ${ARQUIVO}
 alt_d "smtp_tls_wrappermode" " = " "yes" ${ARQUIVO}
else
 alt_d "smtp_tls_security_level" " = " "may" ${ARQUIVO}
 alt_d "smtp_tls_wrappermode" " = " "no" ${ARQUIVO}
fi

alt_d "mydestination" " = " "localhost.${SMTP_DOMAIN}, localhost" ${ARQUIVO}
alt_d "sender_canonical_maps" " = " "hash:/etc/postfix/sender_canonical" ${ARQUIVO}
alt_d "recipient_canonical_maps" " = " "hash:/etc/postfix/recipient_canonical" ${ARQUIVO}

ARQUIVO="/etc/postfix/sasl/smtplw_passwd"
echo "Configurando ${ARQUIVO}"
echo "[${SMTP_SERVER}]:${SMTP_PORT} ${SMTP_USER}:${SMTP_PASS}" > ${ARQUIVO}
chmod 0600 ${ARQUIVO}
postmap ${ARQUIVO}

ARQUIVO="/etc/postfix/sender_canonical"
echo "Configurando ${ARQUIVO}"
[ ! -f ${ARQUIVO} ] && touch ${ARQUIVO}
alt_d "root" " " "${SMTP_MAIL_FROM}" ${ARQUIVO}
postmap ${ARQUIVO}

ARQUIVO="/etc/postfix/recipient_canonical"
echo "Configurando ${ARQUIVO}"
[ ! -f ${ARQUIVO} ] && touch ${ARQUIVO}
alt_d "root" " " "${SMTP_MAIL_TO}" ${ARQUIVO}
postmap ${ARQUIVO}

## limpeza da fila postQUEUE -p
postsuper -d ALL

echo "Reiniciando serviços do postfix"
systemctl restart postfix
